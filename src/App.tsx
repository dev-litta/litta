import React from 'react'
import logo from './logo.svg'
import './App.scss'
import { DatePicker } from './components/date-picker'

function App() {
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <h1>Litta</h1>
                <DatePicker title="What day is best for you?"></DatePicker>
            </header>
        </div>
    )
}

export default App
