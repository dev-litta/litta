import { DatePickerDateModel } from '../models/date-picker-date.model'
import './date-picker-card.scss'

interface IDatePickerCard {
    date: DatePickerDateModel
    selectDateHandler: (newSelectedDate: DatePickerDateModel) => void
}

export const DatePickerCard = ({
    date,
    selectDateHandler,
}: IDatePickerCard) => {
    const isSelected = date.isSelected ? 'litta-date-picker-card__selected' : ''
    const isAvailable = date.isAvailable
        ? 'litta-date-picker-card__available'
        : 'litta-date-picker-card__non-available'
    const classes = `litta-date-picker-card__date-slot ${isSelected} ${isAvailable}`

    return (
        <div className="litta-date-picker-card">
            <div className="litta-date-picker-card__week-slot">
                <span>{date.dayOfTheWeek}</span>
            </div>
            <div className={classes} onClick={() => selectDateHandler(date)}>
                <span>{date.day}</span>
                <span>{date.month}</span>
            </div>
        </div>
    )
}
