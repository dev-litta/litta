import moment from 'moment'
interface ISlotsPicker {
    title: string
}

export const SlotsPicker = ({ title }: ISlotsPicker) => {
    const currentTime = moment().format('hh:mm A')
    return (
        <div className="litta-hour-picker">
            <div>
                <h3 className="litta-hour-picker__title">{title}</h3>
                <h5>Europe/London Time ({currentTime})</h5>
            </div>
        </div>
    )
}
