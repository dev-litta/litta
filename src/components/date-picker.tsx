import React, { useState, useEffect } from 'react'
import moment, { Moment } from 'moment'
import {
    BsFillArrowLeftCircleFill,
    BsFillArrowRightCircleFill,
} from 'react-icons/bs'

import { SlotsPicker } from './slots-picker-card'
import { DatePickerDateModel } from '../models/date-picker-date.model'
import { DatePickerCard } from './date-picker-card'
import './date-picker.scss'
import { SlotPickerHourModel } from '../models/slot-picker-hour.model'

interface IDatePicker {
    title: string
}
const defaultFormatDate = 'DD-MM-YY'

const generateSlots = (day: Moment) => {
    const slotsList: SlotPickerHourModel[] = []
    const startHour = 8
    const endHour = 22

    for (let i = startHour; i < endHour; i++) {
        slotsList.push({
            id: `${day.format(defaultFormatDate)}-${i}-00`,
            hour: `${i}:00`,
            day: `${day.format('DD')}`,
            month: `${day.format('MM')}`,
            year: `${day.format('YY')}`,
            isAvailable: true,
        })
        slotsList.push({
            id: `${day.format(defaultFormatDate)}-${i}-30`,
            hour: `${i}:30`,
            day: `${day.format('DD')}`,
            month: `${day.format('MM')}`,
            year: `${day.format('YY')}`,
            isAvailable: true,
        })
    }
}

export const DatePicker = ({ title }: IDatePicker) => {
    const [dates, setDates] = useState<DatePickerDateModel[]>([])
    const [selectedDate, setSelectedDate] = useState(
        moment().format(defaultFormatDate)
    )
    const [selectedWeek, setSelectedWeek] = useState(moment().week())
    const currentDate: Moment = moment()
    const currentWeek = currentDate.week()

    const weekHandler = (direction: string) => {
        if (direction === 'backward' && currentWeek > selectedWeek - 1) {
            return
        }
        dates.length = 0

        switch (direction) {
            case 'backward':
                generateDates(selectedWeek - 1)
                break
            case 'forward':
                generateDates(selectedWeek + 1)
                break
            default:
                break
        }
    }

    const generateDates = (week: number): void => {
        setSelectedWeek(week)
        if (dates.length) {
            return
        }

        // const weekStart = currentDate.clone().startOf('isoWeek')
        const weekStart = moment().day('Monday').week(week)

        const newDatesList: DatePickerDateModel[] = []
        for (let i = 0; i <= 6; i++) {
            const day = moment(weekStart).add(i, 'days')

            newDatesList.push({
                id: day.format(defaultFormatDate),
                day: day.format('DD'),
                dayOfTheWeek: day.format('ddd'),
                month: day.format('MMM'),
                isSelected: selectedDate === day.format(defaultFormatDate),
                isAvailable: currentDate <= day,
                slots: [],
            })
        }
        setDates([...newDatesList])
    }

    useEffect(() => {
        generateDates(currentWeek)
    }, [])

    const selectDateHandler = (newSelectedDate: DatePickerDateModel): void => {
        if (!newSelectedDate.isAvailable) {
            return
        }
        setSelectedDate(newSelectedDate.id)

        dates.map((d) =>
            d.day === newSelectedDate.day
                ? (d.isSelected = true)
                : (d.isSelected = false)
        )

        setDates([...dates])
    }

    return (
        <div className="litta-date-picker">
            <h3 className="litta-date-picker__title">{title}</h3>
            <div className="litta-date-picker__wrapper">
                <div className="litta-date-picker__week-arrow-wrapper">
                    <BsFillArrowLeftCircleFill
                        className="litta-date-picker__week-arrow"
                        size={36}
                        onClick={() => weekHandler('backward')}
                    />
                </div>
                <div className="litta-date-picker__cards-wrapper">
                    {dates.map((d) => (
                        <DatePickerCard
                            key={d.id}
                            date={d}
                            selectDateHandler={selectDateHandler}
                        ></DatePickerCard>
                    ))}
                </div>
                <div className="litta-date-picker__week-arrow-wrapper">
                    <BsFillArrowRightCircleFill
                        className="litta-date-picker__week-arrow"
                        size={36}
                        onClick={() => weekHandler('forward')}
                    />
                </div>
            </div>

            <div>
                <SlotsPicker title="What time works?"></SlotsPicker>
            </div>
        </div>
    )
}
