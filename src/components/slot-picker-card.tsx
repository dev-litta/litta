import { SlotPickerHourModel } from '../models/slot-picker-hour.model'

interface ISlotsPickerCard {
    title: string
    slot: SlotPickerHourModel
}

export const SlotsPickerCard = ({ title, slot }: ISlotsPickerCard) => {
    return (
        <div className="litta-hour-picker">
            <h3 className="litta-hour-picker__title">{title}</h3>
            <h5>Europe/London Time ({slot.hour})</h5>
        </div>
    )
}
