import { SlotPickerHourModel } from './slot-picker-hour.model'

export interface DatePickerDateModel {
    id: string
    day: string
    dayOfTheWeek: string
    month: string
    isSelected: boolean
    isAvailable: boolean
    slots: SlotPickerHourModel[]
}
