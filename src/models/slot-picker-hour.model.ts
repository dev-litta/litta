export interface SlotPickerHourModel {
    id: string
    hour: string
    day: string
    month: string
    year: string
    isAvailable: boolean
}
